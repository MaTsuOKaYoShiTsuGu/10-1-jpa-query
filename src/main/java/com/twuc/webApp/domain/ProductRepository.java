package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> getByProductLine(List<ProductLine> productLineList);
    List<Product> getByQuantityInStockBetween(Short quantityInStock, Short quantityInStock2);
    Page<Product> getByQuantityInStockBetween(Short quantityInStock, Short quantityInStock2, Pageable pageable);
    // --end-->
}