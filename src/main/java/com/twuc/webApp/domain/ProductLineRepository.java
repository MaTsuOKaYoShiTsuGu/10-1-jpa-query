package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.Size;
import java.util.List;

public interface ProductLineRepository extends JpaRepository<ProductLine, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    ProductLine getByProductLine(String productLine);
    List<ProductLine> getByTextDescriptionContains(@Size(max = 4000) String textDescription);
    // --end-->
}
